const express = require('express');
const config = require('config');
const app = express();
const myDb = config.get('db');
require('./startup/validation')();
require('./startup/db')(myDb);
require('./startup/logging')(myDb);
require('./startup/routes')(app);
require('./startup/config')();
require('./startup/prod')(app);

app.get('/', (req,res)=>{
    res.send('Server is alive');
});

const server = app.listen(3000, ()=>console.log("LISTENING ON 3000"));

module.exports = server;

