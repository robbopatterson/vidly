const joi = require('joi');
const mongoose = require('mongoose');

const customerSchema =  new mongoose.Schema({
    name: { type: String, required: true },
    isGold: { type: Boolean },
    phone: { type: String }
});

const joiSchema = {
    name: joi.string().required().min(1),
    isGold: joi.boolean(),
    phone: joi.string()
};

const Customer = mongoose.model('Customer', customerSchema);

function validate(customer) {
    return joi.validate(customer,joiSchema);
}


module.exports = {
    Customer, validate, customerSchema
};