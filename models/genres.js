const joi = require('joi');
const mongoose = require('mongoose');

const genreSchema =  new mongoose.Schema({
    name: String
});

const genreJoiSchema = {
    name: joi.string().required().min(1)
};

const Genre = mongoose.model('Genre', genreSchema);

function validate(genre) {
    return joi.validate(genre,genreJoiSchema);
}

module.exports = {Genre, validate, genreSchema};
