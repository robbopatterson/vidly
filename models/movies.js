const joi = require('joi');
const mongoose = require('mongoose');
const {genreSchema} = require('./genres');

const joiSchema = {
    title: joi.string().required(),
    genreId: joi.objectId().required(),
    numberInStock: joi.number().required().min(0),
    rentalRate: joi.number().required().min(0),
};

const movieSchema =  new mongoose.Schema({
    title: String,
    numberInStock: {
        type: Number,
        min: 0,
        required: true
    },
    rentalRate: {
        type: Number,
        require: true,
        min: 0
    },
    genre:
    {
        type: genreSchema,
        required: true
    }

});

const Movie = mongoose.model('Movie', movieSchema);

function validate(movie) {
    return joi.validate(movie,joiSchema);
}

module.exports = {Movie, validate, movieSchema};
