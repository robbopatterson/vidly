const Joi = require('joi');
const mongoose = require('mongoose');
const {movieSchema} = require('./movies');
const {customerSchema} = require('./customers');

const rentalSchema =  new mongoose.Schema({
    dateOut: {
        type: Date
    },
    dateReturned: {
        type: Date
    },
    rentalFee: {
        type: Number
    },
    movie:
    {
        type: new mongoose.Schema({
            _id: String,
            title: String
        }),
        required: true
    },
    customer:
    {
        type: new mongoose.Schema({
            _id: String,
            name: String
        }),
        required: true
    }
});

const joiSchema = {
    movieId: Joi.objectId().required(),
    customerId: Joi.objectId().required()
};

const Rental = mongoose.model('Rental', rentalSchema);

function validate(rental) {
    return Joi.validate(rental,joiSchema);
}

module.exports = {Rental, validate};
