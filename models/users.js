const Joi = require('joi');
const mongoose = require('mongoose');
const config = require('config');
const jwt = require('jsonwebtoken');

const joiSchema = {
    name: Joi.string().required(),
    email: Joi.string().required(),
    password: Joi.string().required()
};
function validate(user) {
    return Joi.validate(user,joiSchema);
}

let userSchema =  new mongoose.Schema({
    name: { type: String, required: true },
    email: { type: String, required: true, unique: true },
    password: { type: String, required: true },
    isAdmin: { type: Boolean }
});
userSchema.methods.generateAuthToken = function() {
    console.log("this",this);
    return jwt.sign( {_id: this._id, isAdmin: this.isAdmin}, config.get('jwtPrivateKey'));
};
const User = mongoose.model('User', userSchema);


module.exports = {User, validate};
