const _ = require ('lodash');
const config = require('config');
const jwt = require('jsonwebtoken');
const express = require('express');
const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const Joi = require('joi');
const {User} = require('../models/users');

const router = express.Router();

function validate(user) {
    return Joi.validate(user,{
        email: Joi.string().required(),
        password: Joi.string().required()
    });
}

router.post('/', async (req,res)=>{
 
    var {error} = validate(req.body);
    if (error) return await res.status(400).send(error.message);

    let user = await User.findOne( {email:req.body.email} );
    if (!user) return await res.status(400).send("Invalid email or password");

    const isValid = await bcrypt.compare(req.body.password,user.password);
    if (!isValid) return await res.status(400).send("Invalid email or password");

    return res.send(user.generateAuthToken());
});

router.delete('/:id', async (req,res)=>{
    const user = await User.deleteOne({_id:req.params.id});
    if (!user)
        res.status(404).send("Not found");
    res.send(user);
});

module.exports = router;