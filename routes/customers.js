const express = require('express');
const mongoose = require('mongoose');
const { Customer, validate } = require('../models/customers');
const router = express.Router();


router.get('/',  (req,res)=>{
    const result =  Customer.find().then( cs => res.send(cs));
});
router.get('/:id',  (req,res)=>{
    Customer.find({_id:req.params.id})
    .then(customer=>res.send(customer))
    .catch(err=>res.status(404).send("Not found"));
});


router.post('/',  (req,res)=>{

    var {error} = validate(req.body);
    if (error) return res.status(400).send(error.message);

    const newCustomer = new Customer({
        name: req.body.name,
        isGold: req.body.isGold,
        phone: req.body.phone,
    });

    newCustomer.save();

    res.send(newCustomer);
});

router.put('/:id',  (req,res)=>{
    const customer = Customer.find({_id:req.params.id})
    .then(customer=>{
        var {error} = validate(req.body);
        if (error) return res.status(400).send(error.message);
    
        const newCustomer = {
            _id: req.params.id,
            name: req.body.name,
            isGold: req.body.isGold,
            phone: req.body.phone,
        };
    
        newCustomer.save();
    
        res.send(newCustomer);
    })
    .catch(err=>res.status(404).send("Not found"))
});


router.delete('/:id',  (req,res)=>{
    Customer.deleteOne({_id:req.params.id})
    .then(customer=>res.send(customer))
    .catch(err=>res.status(400).send(error.message));
});
module.exports = router;