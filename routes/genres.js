const express = require('express');
const mongoose = require('mongoose');
const {Genre, validate} = require('../models/genres');
const auth = require('../middleware/auth');
const admin = require('../middleware/admin');

const router = express.Router();


router.get('/', async (req,res)=>{
    const genres = await Genre.find();
    res.send(genres);
});

router.get('/:id',  (req,res)=>{
    Genre.find({_id:req.params.id})
    .then(genre=>res.send(genre))
    .catch(err=>res.status(404).send("Not found"));
});


router.post('/', auth, (req,res)=>{

    var {error} = validate(req.body);
    if (error) return res.status(400).send(error.message);

    const newGenre = new Genre({
        name: req.body.name
    });

    newGenre.save();

    res.send(newGenre);
});

router.put('/:id', auth, (req,res)=>{
    const genre = Genre.find({_id:req.params.id})
    .then(genre=>{
        var {error} = validate(req.body);
        if (error) return res.status(400).send(error.message);

        const newGenre = {
            _id: req.params.id,
            name: req.body.name
        };
    
        newGenre.save();
    
        res.send(newGenre);
    })
    .catch(err=>res.status(404).send("Not found"))
});


router.delete('/:id', [auth,admin], (req,res)=>{
    Genre.deleteOne({_id:req.params.id})
    .then(genre=>res.send(genre))
    .catch(err=>res.status(400).send(error.message));
});

module.exports = router;