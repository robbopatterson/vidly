const express = require('express');
const mongoose = require('mongoose');
const {Movie, validate} = require('../models/movies');
const {Genre} = require('../models/genres');

const router = express.Router();


async function getMovies() {
    const result = await Movie.find();
    console.log(result);
    return result;
}

router.get('/',  (req,res)=>{
    getMovies().then( gs => res.send(gs));
});

router.get('/:id',  (req,res)=>{
    Movie.find({_id:req.params.id})
    .then(movie=>res.send(movie))
    .catch(err=>res.status(404).send("Not found"));
});


router.post('/', async (req,res)=>{

    var {error} = validate(req.body);
    if (error) return res.status(400).send(error.message);

    const genre = await Genre.findById(req.body.genreId);
    if (!genre) return res.status(404).send("Not found");

    let movie = newMovie(req.body);
    movie.genre = {
        _id: genre._id,
        name: genre.name
    };
    
    let m = await movie.save();
    res.send(m);
});

function newMovie(movie) {
    let newMovie = new Movie({
        title: movie.title,
        numberInStock: movie.numberInStock,
        rentalRate: movie.rentalRate,
    });
    return newMovie;    
}

router.put('/:id', async (req,res)=>{
    const movie = await Movie.findById(req.params.id);
    if (!movie) return res.status(404).send("Movie Not found");
    const genre = await Genre.findById(req.body.genreId);
    console.log("genre",genre);
    if (!genre) return res.status(404).send("Genre Not found");
    
    var {error} = validate(req.body);
    if (error) return res.status(400).send(error.message);

    movie.title= req.body.title,
    movie.numberInStock= req.body.numberInStock,
    movie.rentalRate= req.body.rentalRate,
    movie.genre = {
        _id: genre._id,
        name: genre.name
    };
console.log("movie",movie);

    let m =  movie.save();

    res.send(m);
});


router.delete('/:id',  (req,res)=>{
    Movie.deleteOne({_id:req.params.id})
    .then(movie=>res.send(movie))
    .catch(err=>res.status(400).send(error.message));
});

module.exports = router;