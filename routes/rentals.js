const express = require('express');
const mongoose = require('mongoose');
const Fawn = require('fawn');
const {Rental, validate} = require('../models/rentals');
const {Genre} = require('../models/genres');
const {Movie} = require('../models/movies');
const {Customer} = require('../models/customers');

Fawn.init(mongoose);
const router = express.Router();

router.get('/', async (req,res)=>{
    const rentals = await Rental.find();
    res.send(rentals);
});

router.get('/:id', async (req,res)=>{
    console.log("get");
    let rental = await Rental.findById(req.params.id);
    console.log("rental",rental);
    if (!rental)
        res.status(404).send("Rental Not found");
    res.send(rental);
});

router.post('/', async (req,res)=>{
 
    var {error} = validate(req.body);
    if (error) return await res.status(400).send(error.message);
    const movie = await Movie.findById(req.body.movieId);
    if (!movie) return await res.status(404).send("Movie not found");
    const customer = await Customer.findById(req.body.customerId);
    if (!customer) return await res.status(404).send("Customer not found");

    if (movie.numberInStock<1)
        return await res.status(400).send("Movie out of stock");

    let rental = new Rental({
        dateOut: Date.now(),
        dateReturned: req.body.dateReturned,
        rentalFee: movie.rentalRate,
        movie : {
            _id: movie._id,
            title: movie.title
        },
        customer: {
            _id: customer._id,
            name: customer.name
        }
    });
    console.log("rental",rental);
    
    try {
        new Fawn.Task()
        .save('rentals',rental)
        .update('movies', {_id:movie._id}, 
        { 
            $inc: {numberInStock:-1}
        })
        .run();

        res.send(rental);
    } catch (ex) {
        res.status(500).send(`Unexpected Transaction Failure: ${ex}`);
    }

});

router.delete('/:id', async (req,res)=>{
    const rental = await Rental.deleteOne({_id:req.params.id});
    if (!rental)
        res.status(404).send("Not found");
    res.send(rental);
});

module.exports = router;