const _ = require ('lodash');
const express = require('express');
const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const config = require('config');
const jwt = require('jsonwebtoken');
const {User, validate} = require('../models/users');
const auth = require('../middleware/auth');

const router = express.Router();

router.get('/me', auth, async (req,res)=>{
    let user = await User.findById(req.user._id).select('-password');
    if (!user)
        res.status(404).send("User Not found");
    res.send(user);
});

async function hashedAndSalted(value) {
    const salt = await bcrypt.genSalt(10);
    return bcrypt.hash(value,salt);
}

router.post('/', async (req,res)=>{
 
    var {error} = validate(req.body);
    if (error) return await res.status(400).send(error.message);

    let user = await User.findOne( {email:req.body.email} );
    if (user) return await res.status(400).send("Duplicate email");

    let userReq = _.pick(req.body,['name','email','password']);
    userReq.password = await hashedAndSalted(userReq.password);
    user = new User(userReq);
    console.log("user",user);
    
    await user.save();

    const token = user.generateAuthToken();
    res.header('x-auth-token',token).send(_.pick(user,['_id','name','email']));
});

router.delete('/:id', async (req,res)=>{
    const user = await User.deleteOne({_id:req.params.id});
    if (!user)
        res.status(404).send("Not found");
    res.send(user);
});

module.exports = router;