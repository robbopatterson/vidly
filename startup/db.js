const mongoose = require('mongoose');

module.exports = function(connection) {
    mongoose.connect(connection)
    .then( () => console.log(`Vidly Connected to ${connection}`));
}