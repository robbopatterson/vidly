require('express-async-errors');
const winston = require('winston');
//require('winston-mongodb');  // disabled due to problem running with integration tests

module.exports = function(connection){
    //winston.add( winston.transports.MongoDB, {db:connection} );
    process.on( 'unhandledRejection', (ex) => {
        throw ex; // Convert rejection to exception
    });
} 